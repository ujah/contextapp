import React from 'react';
import NavBar from './Components/NavBar'
import BookList from './Components/BookList'
import ThemeContextProvider from './contexts/ThemeContext';
import AuthContextProvider from './contexts/AuthContext';
import ThemeToggle from './Components/ThemeToggle';
import BookContextProvider from './contexts/BookContext';

function App() {
  return (
    <div className="App">
    <ThemeContextProvider>
      <AuthContextProvider>
        <NavBar/>
            <BookContextProvider>
          <BookList/>
        </BookContextProvider>
        <ThemeToggle/>
    </AuthContextProvider>
    </ThemeContextProvider>
    </div>
  );
}

export default App;
