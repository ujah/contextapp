import React, { useContext} from 'react';
import {ThemeContext} from '../contexts/ThemeContext';
import {AuthContext} from '../contexts/AuthContext'

const Navbar = () => {
    const {toggleAuth, isAuthenticated} = useContext(AuthContext);
    const {light, isLightTheme, dark} = useContext(ThemeContext);
    const theme = isLightTheme ? light : dark;

    return ( 
        <nav className="nav" style={{background: theme.ui, color: theme.syntax}}>
            <h1>Context App</h1>
                <div onClick={toggleAuth}>
                    { isAuthenticated ? 'Logged in' : 'Logged out'}
                </div>
                <ul>
                    <li>Home</li>
                    <li>Contact</li>
                    <li>About</li>
                </ul>
        </nav>
     );
    } 
export default Navbar;