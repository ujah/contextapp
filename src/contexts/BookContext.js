import React,{createContext, useState} from 'react';

export const BookContext = createContext();

 const BookContextProvider = (props) => {
     const [books, setbooks] = useState([
         {title:'A voice in the wind', id:1},
         {title:'As sure as the dawn', id:2},
         {title:'Echo in the darkness', id:3},
         {title:'Living in bondage', id:4}
     ])
     return ( 
        <BookContext.Provider value ={{books}}>
                {props.children}
            </BookContext.Provider>
      );
 }
  
 export default BookContextProvider;